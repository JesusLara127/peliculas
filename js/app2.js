function cargarDatos(aux) {
    let res= document.getElementById('lista');
    res.innerHTML="";
    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/albums/"+ aux;
    //console.log(url)
    // función respuesta peticion
    http.onreadystatechange = function () {
        // validar respuesta
        if (this.status == 200 && this.readyState == 4) {
            let res = document.getElementById('lista');
            const json = JSON.parse(this.responseText);
            // mostrar datos json
                res.innerHTML += '<tr> <td class="columna1">' + json.userId + '</td>' +
                    ' <td class="columna2">' + json.id + '</td>' +
                    ' <td class="columna3">' + json.title + '</td> </tr>';
        } 
    };
    http.open('GET', url, true);
    http.send();
}
// botones
document.getElementById("btnBuscar").addEventListener('click', function () {
    let num = document.getElementById("num").value; // Obtener el valor del input "num"
    num = num.toString();
    cargarDatos(num); // Llamar a la función cargarDatos con la nueva URL
});
