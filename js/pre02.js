// JavaScript (pre02.js)

function cargarDatos() {
    const selectElement = document.getElementById('lista');
    const url = "https://dog.ceo/api/breeds/list";

    fetch(url)
        .then(response => {
            if (!response.ok) {
                throw new Error(`Error en la petición: ${response.status}`);
            }
            return response.json();
        })
        .then(data => {
            // Verificar si se obtuvieron razas
            if (data.message.length > 0) {
                mostrarRazas(data.message);
            } else {
                alert("No se encontraron razas.");
                limpiarRazas();
            }
        })
        .catch(error => {
            console.error("Error en la petición:", error);
            const lblError = document.getElementById('lblError');
            lblError.innerHTML = "Surgió un error: " + error.message;
        });
}

function mostrarRazas(razas) {
    const selectElement = document.getElementById('lista');
    // Limpiar opciones existentes
    selectElement.innerHTML = "<option value=''></option>";

    // Agregar las nuevas opciones desde la respuesta de la API
    razas.forEach(raza => {
        const optionElement = document.createElement("option");
        optionElement.value = raza;
        optionElement.textContent = raza;
        selectElement.appendChild(optionElement);
    });
}

function limpiarRazas() {
    const selectElement = document.getElementById('lista');
    // Limpiar opciones existentes
    selectElement.innerHTML = "<option value=''></option>";
    const lblError = document.getElementById('lblError');
    lblError.innerHTML = "";
}

function mostrarImagen() {
    const selectElement = document.getElementById('lista');
    const razaSeleccionada = selectElement.value;

    if (razaSeleccionada) {
        const imagenContainer = document.getElementById('imagenContainer');
        const imgUrl = `https://dog.ceo/api/breed/${razaSeleccionada}/images/random`;

        fetch(imgUrl)
            .then(response => response.json())
            .then(data => {
                if (data.message) {
                    imagenContainer.innerHTML = `<img src="${data.message}" alt="${razaSeleccionada}">`;
                } else {
                    alert("No se encontró la imagen.");
                }
            })
            .catch(error => {
                console.error("Error en la petición de la imagen:", error);
            });
    } else {
        alert("Selecciona una raza antes de ver la imagen.");
    }
}

// Evento click en el botón "Cargar Razas"
document.getElementById("btnCargar").addEventListener('click', function (event) {
    event.preventDefault(); // Evitar el comportamiento predeterminado del formulario
    cargarDatos();
});

// Evento click en el botón "Ver imagen"
document.getElementById("img").addEventListener('click', function (event) {
    event.preventDefault(); // Evitar el comportamiento predeterminado del formulario
    mostrarImagen();
});

