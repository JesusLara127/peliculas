function cargarDatos() {
    const movieNameElement = document.getElementById('movieName');
    const movieName = movieNameElement.value.trim();  
    if (movieName === "") {
        alert("Por favor, ingrese el nombre de una película.");
        return; 
    }
    const url = `https://www.omdbapi.com/?t=${movieName}&plot=full&apikey=30063268`;
    fetch(url)
    .then(respuesta => respuesta.json())
    .then(data => {
        if (data.Title && data.Title.toLowerCase() === movieName.toLowerCase()) {
            mostrarTodos(data);
        } else {
            alert("No se encontraron datos para la película especificada.");
        }
    })
    .catch(reject => {
        const lblError = document.getElementById('lblError');
        lblError.innerHTML = "Surgió un error" + reject;
    });
}
function mostrarTodos(data) {
    const resultNameElement = document.getElementById('resultName');
    const resultYearElement = document.getElementById('resultYear');
    const resultActorsElement = document.getElementById('resultActors');
    const resultPlotElement = document.getElementById('resultPlot');
    const resultPosterElement = document.getElementById('resultPoster');

    resultNameElement.value = data.Title || "";
    resultYearElement.value = data.Year || "";
    resultActorsElement.value = data.Actors || "";
    resultPlotElement.value = data.Plot || "";

    if (data.Poster) {
        resultPosterElement.innerHTML = `<img src="${data.Poster}" alt="Movie Poster">`;
    } else {
        resultPosterElement.innerHTML = "No poster available";
    }
}
// Evento click en el botón "buscar"
document.getElementById("btnBuscar").addEventListener('click', function (event) {
    event.preventDefault();
    cargarDatos();
});
