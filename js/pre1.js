function cargarDatos(aux) {
    let res = document.getElementById('resultados');
    //res.innerHTML = "";

    const url = "https://jsonplaceholder.typicode.com/users/" + aux;

    axios.get(url)
        .then(function (response) {
            const json = response.data;

            const nameInput = document.querySelector('#name');
            const usernameInput = document.querySelector('#username');
            const emailInput = document.querySelector('#email');
            const streetInput = document.querySelector('#street');
            const suiteInput = document.querySelector('#suite');
            const cityInput = document.querySelector('#city');

            if (nameInput) nameInput.value = json.name;
            if (usernameInput) usernameInput.value = json.username;
            if (emailInput) emailInput.value = json.email;
            if (streetInput) streetInput.value = json.address.street;
            if (suiteInput) suiteInput.value = json.address.suite;
            if (cityInput) cityInput.value = json.address.city;
        })
        .catch(function (error) {
            console.error('Error al cargar datos:', error);
        });
}

// Botones
document.getElementById("miFormulario").addEventListener('submit', function (event) {
    event.preventDefault(); // Evitar el comportamiento predeterminado del formulario

    let num = document.getElementById("num").value;

    if (num.trim() === "") {
        // Si el campo está vacío, muestra una alerta
        alert("Ingresa un ID válido");
    } else {
        // Si el campo no está vacío, carga los datos
        num = num.toString();
        cargarDatos(num);
    }
});



