function cargarDatos(aux) {
    const res = document.getElementById('resultados');
    //res.innerHTML = "";
    const url = "https://restcountries.com/v3.1/name/" + aux;

    fetch(url)
        .then(response => {
            if (!response.ok) {
                throw new Error(`Error en la petición: ${response.status}`);
            }
            return response.json();
        })
        .then(data => {
            // Verificar si se encontró el país
            if (data.length > 0) {
                mostrarResultados(data[0]);
            } else {
                alert("No se encontró el país.");
                limpiarResultados();
            }
        })
        .catch(error => {
            console.error("Error en la petición:", error);
            const lblError = document.getElementById('lblError');
            lblError.innerHTML = "Surgió un error: " + error.message;
        });
}

// Botones
document.getElementById("miFormulario").addEventListener('submit', function (event) {
    event.preventDefault(); // Evitar el comportamiento predeterminado del formulario
    let num = document.getElementById("num").value;
    num = num.trim(); // Eliminar espacios en blanco alrededor
    cargarDatos(num);
});

function mostrarResultados(pais) {
    document.getElementById("capital").value = pais.capital || "No disponible";
    document.getElementById("lenguaje").value = pais.languages ? Object.values(pais.languages).join(", ") : "No disponible";
}

function limpiarResultados() {
    document.getElementById("capital").value = "";
    document.getElementById("lenguaje").value = "";
    const lblError = document.getElementById('lblError');
    lblError.innerHTML = "";
}
